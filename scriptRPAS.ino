#include <ESP8266WiFi.h>
#include <ArduinoWebsockets.h>

const char *ssid = "tfg_wifi";   //wifi ssid
const char *password ="tfg_2020_dvelasco";   //wifi password
char *websockets_server_host = "192.168.1.128"; // ip servidor
const uint16_t websockets_server_port = 8080; //puerto del servidor

int reto;
int respuesta;
int xorKey;
using namespace websockets;

//Cambiar valor a true si se quiere que el RPAS envie información
boolean enviarNodo = false;

WebsocketsClient client;


void setup() {
 Serial.begin(115200);
 delay(10);
  
  //comenzamos conectandonos a la red wifi
  WiFi.begin(ssid, password);

  //Wait some time to connect to wifi
  for (int i = 0; i < 10 && WiFi.status() != WL_CONNECTED; i++){
    Serial.print(".");
    delay(1000);
  }

  if(WiFi.status() != WL_CONNECTED){
    Serial.println("La conexión ha fallado");
    return;
  }

  Serial.print("Conectado a: ");
  Serial.print(ssid);
  Serial.println("");
  Serial.println("Conectando con el servidor");

  //conexión con servidor websocket

  bool connected = client.connect(websockets_server_host, websockets_server_port, "/");
  if(connected){
    Serial.println("Conexion con el servidor establecida");    
  }else{
    Serial.println("No se ha podido establecer la conexion con el servidor");
  }

  //callback cuando se reciben mensajes
  client.onMessage([&](WebsocketsMessage message) {
    
    if(message.data().toInt() >= 1 && message.data().toInt() <= 10){
      reto = message.data().toInt();
      Serial.print("El reto es: ");
      Serial.println(reto);
      respuesta = puf(reto);
      Serial.print("La respuesta es: ");
      Serial.println(respuesta);
      xorKey = trifork(respuesta);
      if(xorKey < 0)
        xorKey = abs(xorKey);
      Serial.print("la clave es: ");
      Serial.println(xorKey);

      if(enviarNodo){
      String mensaje = "Hola desde el RPAS";
      int longmensaje = mensaje.length() + 1;
      char mensaje_a_enviar [longmensaje];
      mensaje.toCharArray(mensaje_a_enviar, longmensaje);
      encryptDecrypt(mensaje_a_enviar, xorKey, 1);
      } 
    }
    else{
      String receivedString = message.data();           
      unsigned int strlength = message.data().length() + 1;           
      Serial.println("Texto cifrado: ");
      Serial.println(receivedString);
      char inputString [strlength];
              
      receivedString.toCharArray(inputString, strlength);
      Serial.println("Texto descifrado: ");    
      encryptDecrypt(inputString, xorKey, 0);
    }
  });
}
void loop() {
  //let the websockets client check for incoming messages
  if (client.available()){
    client.poll();
  }
  delay(500);
}


//definicion de funciones


//Funcion para simular una funcion fisica no clonable
int puf (int challenge){
  int output;
  switch(challenge){
    case 3: 
      output = challenge + 2;
      break;
    case 5: 
      output = challenge + 3;
      break;
    case 9:
      output = challenge + 1;
      break;
    default:
      output = 3;
      break;   
  }
  return output;
}

//Generador trifork

int trifork(int input){

  int xn, yn, zn, wn, xn_, yn_, zn_;
  
  //valores para x0,y0 y z0
  xn = input;
  yn = input;
  zn = input;

  int m = 4;
  int d = 5;
  
  int r1 = 4;
  int r2 = 7;
  int r3 = 2;
  
  int s1 = 5;
  int s2 = 8;
  int s3 = 6;

  xn_ =((xn-r1 + xn-s1) % m) >> d;
  yn_ =((yn-r2 + yn-s2) % m) >> d;
  zn_ =((zn-r3 + zn-s3) % m) >> d;
  
  xn = ((xn-r1 + xn-s1) %m) ^ zn_;
  yn = ((yn-r2 + xn-s2) %m) ^ xn_;
  zn = ((xn-r3 + xn-s3) %m) ^ yn_;

  wn = xn ^ zn;

  return wn; 
} 



// The same function is used to encrypt and 
// decrypt 
void encryptDecrypt(char inpString[], int key, int enviar_nodo) 
{ 
    // Define XOR key 
    // Any character value will work
    String aux = String(key);        
    char xorKey = aux.charAt(0);   
    //calculate length of input string 
    int len = strlen(inpString); 
  
    // perform XOR operation of key 
    // with every caracter in string 
    for (int i = 0; i < len; i++){ 
        inpString[i] = inpString[i] ^ xorKey; 
        Serial.print(inpString[i]);
    }
    if(enviar_nodo)
      client.send(inpString);    
} 
